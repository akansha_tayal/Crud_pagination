<?php
namespace Excellence\Crud\Api;

use Excellence\Crud\Api\Data\crudInterface;
use Magento\Framework\Api\SortOrder;
use Magento\Framework\Api\SearchCriteriaInterface;

interface crudRepositoryInterface 
{
    public function save(crudInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(crudInterface $page);

    public function deleteById($id);
}
