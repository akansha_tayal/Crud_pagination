<?php
namespace Excellence\Crud\Model\ResourceModel\crud;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Excellence\Crud\Model\crud','Excellence\Crud\Model\ResourceModel\crud');
    }
}
