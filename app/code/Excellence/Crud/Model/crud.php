<?php
namespace Excellence\Crud\Model;
class crud extends \Magento\Framework\Model\AbstractModel implements \Excellence\Crud\Api\Data\crudInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'excellence_crud_crud';

    protected function _construct()
    {
        $this->_init('Excellence\Crud\Model\ResourceModel\crud');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
